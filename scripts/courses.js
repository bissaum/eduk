// controller: courses
app.controller('courses', function($scope, $http, url){

  $scope.courses = [];

  $http({
    method: 'GET',
    url: url.api('courses')
  })
  .then(function(response){

    if(response.status==200){
      $scope.courses = response.data.courses;
    }

  });
});

app.filter('published', function(){
  return function(input){
    var out = [];
    for(var i in input){
      if(input[i].status == 'published'){
        out.push(input[i]);
      }
    }
    return out;
  }
});