'use strict';

// module: eduk
var app = angular.module('eduk', ['ngRoute']);

// environments
app.factory('env', function(){
  return {

    // development environment
    'eduk.dev': {
      app: 'http://eduk.dev/',
      api: 'http://api.eduk.dev/'
    },

    // homologation environment
    'eduk.qa': {
      app: 'http://eduk.qa/',
      api: 'http://api.eduk.qa/'
    },

    // production environment
    'eduk.com.br': {
      app: 'http://eduk.com.br/',
      api: 'http://api.eduk.com.br/'
    }
  }
});

// url service
app.service('url', function(env){

  // baseurl
  this.app = function(path){
    return env[window.location.hostname].app + path;
  }

  // web service
  this.api = function(path){
    return env[window.location.hostname].api + path + '.json';
  }
});

// config routing
app.config(function($routeProvider){

  // route: /courses
  $routeProvider.when('/courses', {
    templateUrl: '/views/courses.html',
    controller: 'courses'
  });

  // route initial
  $routeProvider.otherwise('/courses');
});

// rating
app.directive('rating', function(){

  return {
    link: function(scope, elem, attr){

      var i = 1,
          rating = attr.value*1,
          max = 5,
          html = '';

      while(i<=max){
        if(i<=rating) html += '<i class="fa fa-star"></i>';
        else html += '<i class="fa fa-star-o"></i>';
        i++;
      }

      elem.html(html);
    },
    template: '<p>{{rating}}</p>'
  };
});

// fallback image
app.directive('fallback', function(){
  return {
    link: function(scope, elem, attr){
      elem.bind('error', function(){
        attr.$set('src', attr.fallback);
      });

      attr.$observe('ngSrc', function(value){
        attr.$set('src', attr.fallback);
      });
    }
  }
});

// preview
app.directive('preview', function(){
  return {
    link: function(scope, elem, attr){
      elem.bind('click', function(){

        var modal = document.createElement('div'),
            id = attr.preview.replace(/.*?v=/g,''),
            url = 'https://www.youtube.com/embed/'+id+'?rel=0&showinfo=0&autoplay=1&controls=0',
            html = '<iframe src="'+url+'" frameborder="0" allowfullscreen></iframe>'

        html += '<a id="close" href="#"><i class="fa fa-2x fa-remove"></i></a>';

        modal.id = 'modal';
        modal.innerHTML = html;

        document.body.appendChild(modal);

        document.getElementById('close').onclick = function(){
          var element = document.getElementById('modal');
          element.parentNode.removeChild(element);
        }
      });
    }
  }
});