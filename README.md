# Software engineer test

### Requirements

- [Git](https://git-scm.com/)
- [Nginx](https://www.nginx.com/)
- [NodeJS](https://nodejs.org)
- [Gulp](http://gulpjs.com/)
- [AngularJS](https://angularjs.org/)
- [Bootstrap](http://getbootstrap.com/)
- [Font Awesome](http://fontawesome.io/)

### To build
To build, be sure you have [node](https://nodejs.org) installed. Clone the project:

    git clone https://gitlab.com/bissaum/eduk.git

Then in the cloned directory, simply run:

    npm install

You must also have `gulp` installed globally:

    npm install -g gulp

Running the project test:

	gulp

Create Nginx file in `/usr/local/etc/nginx/sites-enabled/eduk.conf`

    server {
        listen          80;
        server_name     eduk.dev;
        root            /var/www/eduk;
        charset         utf-8;
    }
    
    server {
        listen          80;
        server_name     api.eduk.dev;
        root            /var/www/eduk/mockdata;
        charset         utf-8;
        location / {
            add_header 'Access-Control-Allow-Origin' 'http://eduk.dev';
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET';
        }
    }

Edit hosts file in `/etc/hosts`
    
    sudo vim /etc/hosts

Including in last line

    127.0.0.1       eduk.dev api.eduk.dev

Run in your browser:

    http://eduk.dev

### Author

Robson Pinto <bissaum@gmail.com>
