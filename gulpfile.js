'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');

gulp.task('default', ['watch']);

gulp.task('styles', function(){

  return gulp.src('styles/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('eduk.css'))
    .pipe(gulp.dest('styles'));
});

gulp.task('scripts', function(){

  return gulp.src([
    'node_modules/angular/angular.min.js',
    'node_modules/angular-route/angular-route.min.js',

    'scripts/app.js',
    'scripts/courses.js'
  ])
  .pipe(concat('eduk.js'))
  .pipe(gulp.dest('scripts'))
});

gulp.task('watch', function(){
  gulp.watch('styles/*.sass', ['styles']);
  gulp.watch('scripts/*.js', ['scripts']);
});